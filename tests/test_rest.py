"""
REST API end points unit tests
"""

from unittest.mock import patch, MagicMock
from uuid import uuid4

import pytest
from starlette.testclient import TestClient
from starlette import status

from vanalyzer.config import get_queue, get_results
from vanalyzer.core.models import Registry, Package
from vanalyzer.services.rest import app
from vanalyzer.services.worker import JobStatus, Job

client = TestClient(app)


# Mocking references
_compute_dependencies = "vanalyzer.services.rest.compute_dependencies"
_uuid = "vanalyzer.services.rest.uuid4"
_load_job_schema = "vanalyzer.services.worker.Job.parse_obj"

_AN_UUID = "ef382780-5c42-4408-a2de-d1d079392921"
_FORGED_JOB = Job(id=uuid4(), package="", version="", registry=Registry.npm)


def test_analyze_unsupported_registry():
    r = client.post("/v1/analyze/foo/bar/biz")
    assert r.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


def test_analyze_supported_registry():
    with patch(_compute_dependencies) as compute_dependencies, patch(_uuid, return_value=_AN_UUID):
        queue_mock = MagicMock()
        app.dependency_overrides[get_queue] = lambda: queue_mock

        r = client.post("/v1/analyze/npm/foo/bar", allow_redirects=False)
        compute_dependencies.delay.assert_called_once_with(_AN_UUID)
        queue_mock.set.assert_called_once()
        assert r.status_code == status.HTTP_202_ACCEPTED
        assert r.headers["Location"].endswith(f"/v1/queue/{_AN_UUID}")

        app.dependency_overrides = {}


@pytest.mark.parametrize(
    "job_status, exists, http_status, redirected",
    [
        (JobStatus.Pending, True, 200, False),
        (JobStatus.Started, True, 200, False),
        (JobStatus.Completed, True, 303, True),
        (None, False, 404, False),
    ],
)
def test_progress_status(job_status, exists, http_status, redirected):
    queue_mock = MagicMock()
    queue_mock.get.return_value = _FORGED_JOB.json()
    queue_mock.__contains__.return_value = exists
    app.dependency_overrides[get_queue] = lambda: queue_mock

    with patch(_load_job_schema, return_value=_FORGED_JOB.copy(update={"status": job_status})):
        r = client.get(f"/v1/queue/{_AN_UUID}", allow_redirects=False)
        assert r.status_code == http_status

        if redirected:
            assert "Location" in r.headers
            assert r.headers["Location"] == f"/v1/result/{_AN_UUID}"
        else:
            assert "Location" not in r.headers

    app.dependency_overrides = {}


@pytest.mark.parametrize(
    "store_contains, http_status", [(True, 200), (False, 404)],
)
def test_result(store_contains, http_status):
    results_mock = MagicMock()
    results_mock.__contains__.return_value = store_contains
    results_mock.get.return_value = Package(name="", specs=[], versions=[]).json()

    app.dependency_overrides[get_results] = lambda: results_mock
    r = client.get(f"/v1/result/{_AN_UUID}")
    assert r.status_code == http_status

    app.dependency_overrides = {}
