from unittest.mock import patch, Mock, MagicMock

import pytest
from semantic_version import NpmSpec, Version

from vanalyzer.core.resolver_npm import NpmResolver

_npm_logger = "vanalyzer.core.resolver_npm.logger"


@pytest.mark.parametrize(
    "text_spec, resolved_spec",
    [
        ("1.2.3", NpmSpec("1.2.3")),
        (">1.2.3", NpmSpec(">1.2.3")),
        ("~ 1.2.3", NpmSpec("~1.2.3")),
        ("~    1.2.3", NpmSpec("~1.2.3")),
        ("> 1.2.3", NpmSpec(">1.2.3")),
        (">1.2.3      <2.0.0", NpmSpec(">1.2.3 <2.0.0")),
        ("==1.2.3", NpmSpec("1.2.3")),
        ("=1.2.3", NpmSpec("1.2.3")),
        ("0.1.1rc6", NpmSpec("0.1.1-rc6")),
        ("0.1.1alpha4", NpmSpec("0.1.1-alpha4")),
    ],
)
def test_parse_spec(text_spec, resolved_spec):
    """
    Check that non standard version string are properly handled by the system
    """
    assert NpmResolver.parse_spec(text_spec) == resolved_spec


@pytest.mark.parametrize(
    "text_spec", ["git+https://github.com/act/pkg.git", "github:act/pkg", "github:act/pkg.-js"],
)
def test_erroneous_parse_spec(text_spec):
    """
    Check that unmanageable versions are skipped with an exception
    """
    with pytest.raises(ValueError):
        NpmResolver.parse_spec(text_spec)


def test_not_ok_download_specs():
    """
    Check that a not found package doesn't make the whole system crash
    """
    with patch("requests.get", return_value=Mock(ok=False)), patch(_npm_logger) as npm_logger:
        NpmResolver()._sync_registry_package("foo")
        npm_logger.warning.assert_called_once_with("Package not found: %s", "foo")


@pytest.mark.parametrize(
    "package, version, outcome",
    [
        ("foo", None, "npm:foo"),
        ("foo", "1.2.3", "npm:foo:1.2.3"),
        ("foo", Version("1.2.3"), "npm:foo:1.2.3"),
    ],
)
def test_build_key(package, version, outcome):
    assert NpmResolver()._build_key(package, version) == outcome


@pytest.mark.parametrize(
    "conservative, spec, outcome, is_latest",
    [
        (False, None, [Version("3.0.0")], False),
        (True, None, [Version("1.0.0"), Version("2.0.0"), Version("3.0.0")], False,),
        (False, NpmSpec("<2.0.0"), [Version("1.0.0")], False,),
        (False, NpmSpec("<=2.0.0"), [Version("2.0.0")], False,),
        (True, NpmSpec("<=2.0.0"), [Version("1.0.0"), Version("2.0.0")], False,),
        (False, NpmSpec("<=2.0.0"), [Version("3.0.0")], True,),
        (True, NpmSpec("<=2.0.0"), [Version("3.0.0")], True,),
    ],
)
def test_get_versions(conservative, spec, outcome, is_latest):
    mock_ = MagicMock()
    mock_.get.return_value = ["1.0.0", "2.0.0", "3.0.0"]
    resolver = NpmResolver(cache=mock_, conservative=conservative)
    assert resolver.get_versions("foo", spec, is_latest) == outcome
