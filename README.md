# Dependencies analyzer

![](https://img.shields.io/badge/python-3.8-green.svg)

Really simple system for parsing dependencies existing
among different packages based on public system registries.
In this really simple implementation, cache and storage for
requests are implemented through a simple in memory redis store.

A very simple HTML/CSS/JS front-end is available. For simplicity,
it is served through the same flask app as the REST API, but in real
world scenario, it would be better to serve it through a separate
web server (eg. nginx).

The whole system is implemented the async way using celery and
redis as a message broker for scalability.
The client should pool every x seconds to know if a task is finished.
This may not be the most scalable but still quite easy to implement.

No security mechanism is used in this simple version. The system is
currently only able to process npm dependencies. More test may be added
later on.

## Running

Everything can be run using `docker-compose`. To start the project,
simply run:

```
docker-compose build
docker-compose up
```

Once started, head to your browser to start using the service: [`http://localhost:8000/`](http://localhost:8000/).
The service runs by default on port `8000`.

## Options

Two options are available and can be set at runtime using the environment variables:

- `CONSERVATIVE`: set to `1` to let the program consider all versions includes in a range,
   otherwise will consider only the latest comprised in the range;
- `ALL_SCOPES`: includes as well dev dependencies (experimental, but generally a really
   bad due to the very high number of dependencies)
- `MAX_RECURSION`: change the maximum recursion level on Python interpreter.
  This may be required if requesting huge repos.

## REST API endpoints

There are four available endpoints:

- `/v1/registries` (GET): List the registries the system is able to use;
- `/v1/result/<task_id>` (GET): Get the result of a previously requested task;
- `/v1/queue/<task_id>` (GET): Determine if a requested task has finished;
- `/v1/analyze/<registry>/<package>/<version>` (POST): Request the server to search for the dependencies.

Instead of the front end version, the REST API can be explored through the
minimal swagger definition: `/docs`.

## Tests

Tests can be run directly via docker:

```
docker-compose -f docker-compose.test.yaml build
docker-compose -f docker-compose.test.yaml up
```
