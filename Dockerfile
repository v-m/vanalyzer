FROM python:3.8.1-slim AS base

LABEL maintainer="dev@vincenzomusco.com"

WORKDIR /usr/src/app

RUN pip install poetry
COPY pyproject.toml ./
COPY poetry.lock ./

RUN poetry config virtualenvs.create false
RUN poetry install --no-interaction --no-ansi

COPY vanalyzer ./vanalyzer
COPY static ./static

# Testing
FROM base AS test
#RUN poetry install --no-interaction --no-ansi
COPY tests ./tests
CMD poetry run pytest

# Running
FROM base AS run

COPY bin ./bin
RUN chmod +x ./bin/*

RUN poetry build && pip install dist/*.whl
