"""
Definition of available registry and the resolvers to use with it.
If new resolvers should be defined, add them here.
"""
from typing import Dict

from vanalyzer.config import get_config, get_cache
from vanalyzer.core.models import Registry
from vanalyzer.core.resolver import Resolver
from vanalyzer.core.resolver_npm import NpmResolver

RESOLVERS: Dict[Registry, Resolver] = {
    Registry.npm: NpmResolver(
        conservative=get_config().conservative,
        all_scopes=get_config().all_scopes,
        cache=get_cache(),
    )
}
