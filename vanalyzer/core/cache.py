"""
Available caches that can be used in the system
"""
import json
from abc import ABC

import redis


class Cache(ABC):
    """
    An abstract representation of a version query cache
    """

    def get(self, key):
        raise NotImplementedError

    def put(self, key, value):
        raise NotImplementedError


class RedisCache(Cache):
    """
    Redis-based cache
    """

    def __init__(self, host: str = "localhost", port: int = 6379, db: int = 0, age: int = 15):
        self._server = redis.Redis(host=host, port=port, db=db)
        self._age = age

    def get(self, key: str) -> dict:
        return json.loads(self._server.get(key))

    def put(self, key: str, value: dict):
        self._server.set(key, json.dumps(value))
        self._server.expire(key, self._age * 60)

    def __contains__(self, item: str):
        return item in self._server


class FlatDictCache(Cache):
    """
    A simple in-memory cache without timeout – used for testing purposes
    """

    def __init__(self):
        self._dict = {}

    def get(self, key: str) -> dict:
        return self._dict[key]

    def put(self, key: str, value: dict):
        self._dict[key] = value

    def __contains__(self, item: str):
        return item in self._dict
