"""
Exceptions
"""


class SpecFormatException(Exception):
    def __init__(self, spec):
        self.spec = spec
