"""
npm registry related code
"""

import logging
import re
from functools import lru_cache

import requests
from semantic_version.base import BaseSpec, NpmSpec
from typing import Optional

from vanalyzer.core.models import Registry
from vanalyzer.core.exceptions import SpecFormatException
from vanalyzer.core.resolver import Resolver

logger = logging.getLogger(__file__)

_SPACE_BUG_FIX = re.compile(r"([<>~]=?) *([0-9.\-A-Za-z]+)?")
_RELEASE_INFO_FIX = re.compile(r"(.*?[0-9.]+)([a-zA-Z0-9]+)")
_LATEST_KEYWORD = "latest"


class NpmResolver(Resolver):
    @property
    def registry(self) -> Registry:
        return Registry.npm

    @staticmethod
    @lru_cache
    def parse_spec(version_spec: str) -> Optional[BaseSpec]:
        """
        Parse a version spec into a npm compatible object.
        Perform the needed transformations to make ensure some wrongly formatted version are accepted.
        An example is "<= 2.4.5" where a space should not be accepted between the sign and the version.
        :param version_spec: a version spec as a string
        :return: a parsed NpmSpec
        """
        if version_spec == _LATEST_KEYWORD:
            return None

        spec = version_spec.replace("==", "=")

        components = []
        for g in _SPACE_BUG_FIX.findall(spec):
            components.append("".join(g))
            spec = " ".join(components)

        try:
            return NpmSpec(spec)
        except ValueError:
            # Problem with the spec format
            if matched := _RELEASE_INFO_FIX.match(spec):
                return NpmSpec(f"{matched.group(1)}-{matched.group(2)}")

            raise

    def _iterate_versions(self, package: str, spec: str):
        try:
            parsed_spec = NpmResolver.parse_spec(spec)
        except ValueError:
            raise SpecFormatException(spec)

        for version in self.get_versions(package, parsed_spec, spec == "latest"):
            package_version = self._get_package_info(package, version)
            for scope, iter_over in package_version.items():
                if scope != "main" and not self._all_scopes:
                    continue
                if iter_over:
                    for dependency, dependency_spec in iter_over.items():
                        yield version, dependency, dependency_spec
                else:
                    # If there is no dependency, let's yield the version information to the parent
                    yield version, None, None

    def _sync_registry_package(self, package: str):
        url = "https://registry.npmjs.org/{package}".format(package=package)
        logger.debug("Fetching %s...", url)

        if (request := requests.get(url)).ok:
            payload = request.json()

            if "versions" not in payload:
                payload = {"versions": {}}
        else:
            payload = {"versions": {}}
            logger.warning("Package not found: %s", package)

        versions = list(payload["versions"].keys())
        self._store_versions(package, versions)
        for version, dependencies in payload["versions"].items():
            self._store_package_dependencies(
                package,
                version,
                dependencies.get("dependencies"),
                dependencies.get("devDependencies"),
            )
