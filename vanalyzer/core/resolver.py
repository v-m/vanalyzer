"""
Resolvers are responsible of processing the search and caching entries.
"""
import logging
from abc import ABC, abstractmethod
from typing import Iterable, Optional, List, Union, Set, Tuple, Dict

from semantic_version.base import BaseSpec, Version

from vanalyzer.core.cache import FlatDictCache, Cache
from vanalyzer.core.models import Package
from vanalyzer.core.exceptions import SpecFormatException

logger = logging.getLogger(__file__)


class Resolver(ABC):
    """
    Abstract class for resolvers which fetch their dependencies directly on a external registry.
    Manage the storage of it's discovered dependency with a cache.
    By default, uses a simple naive dictionary cache (non expiring).
    It is recommended to use a more efficient/distributed caching system such as Redis.
    """

    def __init__(self, conservative=False, all_scopes=False, cache: Cache = None):
        self._conservative = conservative
        self._all_scopes = all_scopes
        self._cache = cache or FlatDictCache()
        logger.info("Conservative: %s", self.conservative_enabled)
        logger.info("All scopes: %s", self.all_scope_enabled)
        logger.info("Using cache: %s", self.cache_type)

    @property
    def conservative_enabled(self):
        return self._conservative

    @property
    def all_scope_enabled(self):
        return self._all_scopes

    @property
    def cache_type(self):
        return self._cache.__class__.__name__

    def _build_key(self, package: str, version: Optional[Union[Version, str]] = None):
        """
        A unique key used for caching either a package version index list
        or dependencies related to a specific version.
        :param package: package name
        :param version: package version (None for a version list)
        :return: a unique key to use with a caching system
        """
        return ":".join(
            filter(
                None,
                [
                    self.registry.name,
                    package,
                    str(version) if isinstance(version, Version) else version,
                ],
            )
        )

    def get_versions(
        self, package: str, spec: BaseSpec = None, is_latest: bool = False
    ) -> Iterable[Version]:
        """
        This method returns the list the a specific package versions included in a specific version spec.
        By default, keeps only the most recent comprised in the spec (should be the default behavior of a registry)
        but all versions can be considered using the class property _conservative set via the constructor.
        :param package: the package name
        :param spec: the related spec
        :param is_latest: True if this request is related to the standard "latest"
        :return: a list of versions
        """
        all_versions = [Version(x) for x in self._cache.get(self._build_key(package))]

        if not all_versions:
            return []

        if is_latest:
            return [max(all_versions)]

        ret = [x for x in all_versions if not spec or x in spec]
        if not self._conservative and ret:
            ret = [max(ret)]

        return ret

    def get_specs_for(
        self, package: str, specs: Iterable[str], call_chain: Optional[Set[Tuple[str, str]]] = None
    ) -> Dict:
        """
        Explores recursively a list of version specs and return a dictionary used to build a Package instance.
        This function is called by _search_dependencies in order to build a Package object.
        :param package: the package name
        :param specs: a list of specs to consider
        :param call_chain: used for tracking recursion loops, pass None on the first call
        :return: a dictionary with the considered versions and the dependencies related to this package/specs
        """
        call_chain_ = call_chain or set()
        ret = {}
        versions = set()

        for spec in specs:
            # Check for loops
            if (next_call := (package, spec)) in call_chain_:
                continue
            call_chain_.add(next_call)

            try:
                for version, dependency, dependency_spec in self._iterate_versions(package, spec):
                    if version:
                        versions.add(version)

                    if dependency and dependency_spec:
                        ret.setdefault(dependency, set()).add(dependency_spec)
            except SpecFormatException as e:
                logger.warning("Unable to parse package spec %s for package %s", e.spec, package)

        return {
            "versions": sorted(list(versions), reverse=True),
            "dependencies": [
                self._search_dependencies(dependency, specs, call_chain_)
                for dependency, specs in ret.items()
            ],
        }

    def search_dependencies(self, package: str, spec: str) -> Package:
        """
        Initiate a dependency search
        :param package: the package name
        :param spec: the version as a string
        :return: a Package object describing recursively all dependencies
        """
        return self._search_dependencies(package, [spec])

    def _search_dependencies(self, package, spec: Iterable, call_chain=None) -> Package:
        """
        Synchronize the cached version of a package with the registry and spin the recursive search
        :param package: the package name
        :param spec: a list of spec to consider
        :param call_chain: used for recursion loop prevention. Pass None on first call
        :return: a Package object describing recursively all dependencies
        """
        self._sync_package(package)

        return Package(
            name=package, specs=list(spec), **self.get_specs_for(package, spec, call_chain)
        )

    def _sync_package(self, package: str):
        """
        Ensure that a package is available in the cache
        :param package: a package name
        """
        key = self._build_key(package)

        if key not in self._cache:
            self._sync_registry_package(package)

    def _store_versions(self, package: str, versions: List):
        """
        Store the list of versions into a cache storage
        :param package: the package name
        :param versions: the list of versions available
        """
        key = self._build_key(package)
        self._cache.put(key, versions)

    def _store_package_dependencies(
        self,
        package: str,
        version: Version,
        dependencies: Optional[List],
        other_dependencies: Optional[List],
    ):
        """
        Store the registry raw output for a package/version pair into a cache storage
        :param package: the package name
        :param version: the version
        :param dependencies: it's direct dependencies
        :param other_dependencies: all other dependencies (which would be skipped by default, eg dev dependencies)
        :return:
        """
        key = self._build_key(package, version)
        self._cache.put(key, {"main": dependencies or {}, "other": other_dependencies or {}})

    def _get_package_info(self, package: str, version: Version):
        """
        Request the last version dependencies from the cache storage
        :param package: the package name
        :param version: the package version
        :return:
        """
        return self._cache.get(self._build_key(package, version))

    @property
    @abstractmethod
    def registry(self):
        """
        :return: The registry identity
        """
        raise NotImplementedError

    def _sync_registry_package(self, package: str):
        """
        The concrete implementation of how packages are fetched online from a registry
        :param package: the package name
        """
        raise NotImplementedError

    def _iterate_versions(
        self, package: str, spec: Iterable[str]
    ) -> Iterable[Tuple[Version, Optional[str], Optional[str]]]:
        """
        An iterator yielding the dependencies as they are discovered.
        Yields a tuple of (package_version, dependency_name, dependency_version_spec).
        :param package: he package name
        :param spec: a list of package spec to consider
        :return: a generator
        """
        raise NotImplementedError
