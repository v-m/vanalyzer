"""
Core data structures
"""
from enum import Enum
from typing import List, Tuple, Iterable

from pydantic import BaseModel, Field, validator
from semantic_version import Version


class Registry(Enum):
    """
    List of compatible registries
    """

    npm = "npm"


class Package(BaseModel):
    """
    A specific language/name/version package with it's list of dependencies and acceptable versions
    """

    name: str
    specs: List[str]
    versions: List[Version]
    dependencies: List["Package"] = Field(default_factory=list)

    @validator("versions", pre=True, each_item=True)
    def versions_parse(cls, v):
        if isinstance(v, str):
            return Version(v)
        return v

    def _explore(self):
        yield self
        for dependency in self.dependencies:
            yield from dependency._explore()

    def explore(self) -> Iterable[Tuple[str, str]]:
        """
        Explore all considered versions in the package tree
        :return: a generator yielding each dependence
        """
        seen = set()
        for entry in self._explore():
            for version in entry.versions:
                if (version, entry.name) not in seen:
                    yield entry.name, version
                    seen.add((version, entry.name))

    class Config:
        arbitrary_types_allowed = True
        json_encoders = {
            Version: lambda v: str(v),
        }


Package.update_forward_refs()
