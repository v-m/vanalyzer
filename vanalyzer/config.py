"""
Configuration used by the system.
These variables are simply bound the environment variable to allow a flexible configuration.
"""
import logging
import os
from functools import lru_cache

import redis
from typing import Optional

from pydantic import BaseSettings

from vanalyzer.core.cache import RedisCache


class Config(BaseSettings):
    age_expire: int = 60 * 24
    conservative: bool = False
    all_scopes: bool = False
    max_recursion: Optional[int] = None

    cache_host: str = "localhost"
    cache_port: int = 6379
    cache_db: int = 0

    broker_host: str = "localhost"
    broker_port: int = 6379
    broker_db: int = 1

    queue_host: str = "localhost"
    queue_port: int = 6379
    queue_db: int = 2

    results_host: str = "localhost"
    results_port: int = 6379
    results_db: int = 3


def log_config(logger):
    config = get_config()

    logger.info(
        "Queue – Host: %s, Port: %s, Database: %s",
        config.queue_host,
        config.queue_port,
        config.queue_db,
    )

    logger.info(
        "Result store – Host: %s, Port: %s, Database: %s",
        config.results_host,
        config.results_port,
        config.results_db,
    )

    logger.info(
        "Cache – Host: %s, Port: %s, Database: %s",
        config.cache_host,
        config.cache_port,
        config.cache_db,
    )


@lru_cache(maxsize=1)
def get_config():
    return Config()


def get_queue():
    config = get_config()
    return redis.Redis(config.queue_host, config.queue_port, config.queue_db)


def get_results():
    config = get_config()
    return redis.Redis(config.results_host, config.results_port, config.results_db)


def get_cache() -> RedisCache:
    config = get_config()
    return RedisCache(config.cache_host, config.cache_port, config.cache_db)
