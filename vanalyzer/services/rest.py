"""
REST API for the services.
The REST API is simply responsible of triggering an async job.
"""
import pkgutil
from uuid import uuid4, UUID

from fastapi import FastAPI, status, HTTPException, Response
from fastapi.params import Depends
from redis import Redis
from starlette.responses import HTMLResponse

from vanalyzer.config import get_queue, get_results
from vanalyzer.core.models import Registry, Package
from vanalyzer.services.worker import JobStatus, Job, compute_dependencies

swagger_config = {
    "title": "V-Analyzer",
    "description": "V-Analyzer: explore dependencies existing in a project.",
    "contact": {
        "name": "Vincenzo Musco",
        "email": "dev@vincenzomusco.com",
        "url": "www.vincenzomusco.com",
    },
    "termsOfService": None,
    "version": "1.0.0",
}

app = FastAPI(**swagger_config)


@app.post(
    "/v1/analyze/{registry}/{package}/{version}",
    status_code=status.HTTP_202_ACCEPTED,
    responses={202: {"description": "Request enqueued. See the location header for status"}},
)
def analyze(
    registry: Registry,
    package: str,
    version: str,
    response: Response,
    queue: Redis = Depends(get_queue),
):
    """
    Request to the server to initiate a dependency resolution
    """
    task_id = uuid4()
    job = Job(id=task_id, package=package, version=version, registry=registry)
    queue.set(str(task_id), job.json())
    compute_dependencies.delay(task_id)

    response.headers["Location"] = f"/v1/queue/{task_id}"


@app.get(
    "/v1/queue/{task_id}",
    response_model=Job,
    status_code=status.HTTP_200_OK,
    responses={
        200: {"description": "Task not finished. Find information on the payload."},
        303: {"description": "Task finished. See the location header."},
        404: {"description": "Requested task does not exist"},
    },
)
def progress(
    task_id: UUID, response: Response, queue: Redis = Depends(get_queue),
):
    """
    Check whether a task is finished or not
    """
    task_id_str = str(task_id)

    if task_id_str in queue:
        job = Job.parse_raw(queue.get(task_id_str))

        if job.status == JobStatus.Completed:
            response.headers["Location"] = f"/v1/result/{task_id}"
            response.status_code = status.HTTP_303_SEE_OTHER

        return job
    else:
        raise HTTPException(status.HTTP_404_NOT_FOUND)


@app.get(
    "/v1/result/{task_id}",
    responses={
        200: {"description": "The result of the task"},
        404: {"description": "The requested task does not exist on the server"},
    },
)
def result(task_id: str, results: Redis = Depends(get_results)):
    """
    Get the server produced result for a previous request
    """

    if task_id in results:
        return Package.parse_raw(results.get(task_id))
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)


@app.get("/v1/registries")
def registries():
    """
    List the registries this server is able to query
    """
    return [x.value for x in Registry]


@app.get("/", response_class=HTMLResponse)
def home():
    """
    For simplicity, serve the front end page via the REST API itself.
    However, it would be better to serve static content using a web server.
    """
    return pkgutil.get_data("static", "index.html")
