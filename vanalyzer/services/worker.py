"""
Celery workers used to perform requested tasks.
"""
import sys
from enum import Enum
from uuid import UUID

from celery import Celery
from celery.app.log import Logging
from celery.signals import after_setup_logger
from pydantic import BaseModel

from vanalyzer.availible_resolvers import RESOLVERS
from vanalyzer.config import get_config, get_queue, get_results, log_config
from vanalyzer.core.models import Registry

config = get_config()
queue = get_queue()
results = get_results()


broker_url = f"redis://{config.broker_host}:{config.broker_port}/{config.broker_db}"
app = Celery("celery", broker=broker_url)

logger = Logging(app).get_default_logger()


@after_setup_logger.connect
def setup_loggers(logger, *_args, **_kwargs):
    log_config(logger)

    if config.max_recursion:
        sys.setrecursionlimit(config.max_recursion)
        logger.info("Setting Python max recursion to %d", config.max_recursion)

    logger.info("NPM Registry :: Conservative: %s", RESOLVERS[Registry.npm].conservative_enabled)
    logger.info("NPM Registry :: All scopes: %s", RESOLVERS[Registry.npm].all_scope_enabled)
    logger.info("NPM Registry :: Cache: %s", RESOLVERS[Registry.npm].cache_type)


class JobStatus(Enum):
    # The system acknowledged the query but is not working on it yet
    Pending = "pending"

    # A worker is working on the query at the moment
    Started = "started"

    # The task is completed and it's result can fetched
    Completed = "completed"


class Job(BaseModel):
    """
    A job description as well as it's state
    """

    id: UUID
    package: str
    version: str
    registry: Registry
    status: JobStatus = JobStatus.Pending


@app.task
def compute_dependencies(task_id: UUID):
    task: Job = Job.parse_raw(queue.get(task_id))
    task.status = JobStatus.Started
    queue.set(task_id, task.json())

    ret = RESOLVERS[task.registry].search_dependencies(task.package, task.version)

    task.status = JobStatus.Completed
    queue.set(task_id, task.json())

    results.set(task_id, ret.json())
